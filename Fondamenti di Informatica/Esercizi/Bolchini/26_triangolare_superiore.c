#include <stdio.h>
#define DIM 4

int main(int argc, char * argv[])
{
	int mat[DIM][DIM];
	int i, j, trsup;
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}
	trsup = 1;
	for (i = 0; i < DIM && trsup == 1; i++) {
		for (j = 0; j < DIM && trsup == 1; j++) {
			if (i > j && mat[i][j] != 0) {
				trsup = 0;
			}
		}
	}
	printf ("%d\n", trsup);
	return 0;
}
