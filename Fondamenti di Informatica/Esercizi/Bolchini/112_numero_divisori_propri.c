#include <stdio.h>
#define STOP 0
#define OUTPUTFILE "ris.txt"

int main (int argc, char * argv [])
{
	FILE* fout;
	int val, ris, div, div_validi;

	if ((fout = fopen (OUTPUTFILE, "w"))) {
		div_validi = 0;
		ris = 0;
		scanf ("%d", &val);
		while (val > STOP) {
			for (div = 2; div <= (val / 2); div++) {
				if (!(val % div)) {
					div_validi++;
				}
			}
			fprintf (fout, "%d %d\n", val, div_validi);
			ris++;
			div_validi = 0;
			scanf ("%d", &val);
		}
		printf ("%d\n", ris);
	} else {
		printf ("Errore nell'apertura del file %s\n", OUTPUTFILE);
	}

	return 0;
}	
