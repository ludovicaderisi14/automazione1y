/* Sostituice ogni carattere di una stringa con il carattere seguito dal numero delle occorrnenze (spesso questa è una compressione ma se sono tutte lettere diverse diventa il doppio) */
#include <stdio.h>
#define CMAX 50
#define CMAX_ZIP (CMAX * 2)
#define ZERO_CHAR '0'
#define NUM_NUMERI 9

int main(int argc, char * argv[])
{
	char input[CMAX+1], output[CMAX_ZIP+1], carattere_corrente;
	int i, lenZip, count;

	scanf ("%s", input);

	carattere_corrente = input[0];
	count = 1;
	lenZip = 0;
	for (i = 1; input[i] != '\0'; i++) {
		if (carattere_corrente != input[i] || count == NUM_NUMERI) { /* Appena trovo un carattere diverso lo scrivo nella stringa finale e scrivo anche il valore di count nella posizione dopo, poi dico di aver consumato due posizioni */
			output[lenZip] = carattere_corrente;
			output[lenZip+1] = ZERO_CHAR + count;
			lenZip += 2;

			carattere_corrente = input[i]; /* Ho solo aggiunto l'or perché nel caso entri per quel motivo il carattere che metto in corrente sarà uguale, nel caso entri per l'altro motivo il carattere sarà nuovo come nella versione precedente */
			count = 1;
		} else { /* Se il carattere che sto analizzando è uguale a quello prima incremento count senza scrivere nulla nella stringa di uscita */
			count++;
		}
	}

	output[lenZip] = carattere_corrente;
	output[lenZip+1] = ZERO_CHAR + count;
	output[lenZip+2] = '\0';
	printf("%s\n", &output);
	return 0;
}
