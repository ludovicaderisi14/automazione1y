// Acquisisce un valore, calcola e visualizza "+" se + positivo, "-" se è negativo. "%20" se è 0.

#include <stdio.h>

int main(int argc, char * argv[])
{
	int val;
	char sym;
	
	scanf("%d", &val);

	if(val > 0) { // val == 0 è come dire 0 == val, invece l'assegnamento val = 0 funziona, 0 = val no. Quindi scrivere al contrario quando sono confronti aiuta il compilatore a darti errore
// sbagliare sta cosa qua, oltre a non fare quello che voglio e distruggere la variabile, manda direttamente in else perché lo 0 è associato al falso
		sym = '+'; // singolo apice
	} else {
			if (val < 0) { // da qua
				sym = '-';
			} else {
				sym = ' '; // a qua è un'unica istruzione quindi volendo funziona anche senza graffe
			}
	}
	printf("%c\n", sym); // invece per float il segnaposto è %f o %g
	return 0;
}

// si poteva usare anche #define POS '+' ed è meglio perché se poi voglio cambiare è più facile