/* Scambia a 2 a 2 i numeri in un array */
#include <stdio.h>
#define STOP 0
#define DIM 50

int main(int argc, char * argv[])
{
	int mat[DIM];
	int i, dim, tmp;
	scanf ("%d", &tmp);
	dim = 0;
	while (tmp > STOP && dim < DIM) {
		mat[dim] = tmp;
		dim++;
		scanf ("%d", &tmp);
	}
	for (i = 0; i < dim - 1; i = i + 2) {
		/*if (i + 1 < dim) { Serve per definire quali sono i valori validi (ora è sopra nel for), se l'ultimo è disaccoppiato non va scambiato */
			tmp = mat[i];
			mat[i] = mat[i + 1];
			mat [i + 1] = tmp;
		/*}*/
	}
	for (i = 0; i < dim; i++) {
		printf("%d", mat[i]);
	}
	printf ("\n");
	return 0;
}
