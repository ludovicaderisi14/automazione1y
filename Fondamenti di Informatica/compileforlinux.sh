if [[ $restart2 != y ]]; then
	gccisinstalled=$(apt 2>/dev/null -qq list gcc)
	if [[ $gccisinstalled = *"installed"* ]]
	then
		echo "gcc è installato correttamente"
	else
		echo "Il compilatore gcc non è installato, vuoi installarlo ora? (potresti dover inserire la tua password) [y/n]"
		read authinstallation
		if [[ $authinstallation == y ]]; then
			sudo apt-get install 2>/dev/null gcc
			gccisinstalled=$(apt 2>/dev/null -qq list gcc)
			if [[ $gccisinstalled = *"installed"* ]]; then
				echo "L'installazione automatica è andata a buon fine"
			else
				echo "L'installazione automatica è fallita, potrebbero esserci varie cause, tra cui la mancanza di connessione a internet."
				echo "Attivala e riavvia il programma, altrimenti, se il problema non è questo, cerca su google come installare gcc su linux"
				exit
			fi
		else
			echo "Ok, allora purtroppo non posso aiutarti"
			exit
		fi
	fi
	echo "Premi CTRL+C per uscire dal programma in qualsiasi momento"
	restart=y
fi
while [[ $restart == y ]]; do
	touch tmp.c
	while [[ ! -f tmp.c ]]; do 
		sleep .5
	done
	howmanyfiles=$(ls *.c | wc -l)
	rm tmp.c
	howmanyfiles=$((howmanyfiles-1))
	if [[ $howmanyfiles -gt 1 ]]; then
		echo "C'è più di un file C in questa cartella, inserisci il nome di quello che vuoi compilare nella forma nomefile.c"
		read inputfilename
		while [[ ! -f $inputfilename ]]; do
			echo "Non c'è nessun file chiamato così in questa cartella, ricordati di scrivere il nome del file nella forma nomefile.c"
			echo "Inseriscilo nuovamente"
			read inputfilename
		done
		echo "Come vuoi chiamare il file compilato?"
		read outputfilename
		gcc -O2 -pedantic -std=c89 -Wall -Wno-unused-result -Wno-deprecated-declarations $inputfilename -o $outputfilename -lm
		echo "Vuoi eseguire il file appena compilato? [y/n]"
		read answer
		if [[ $answer == y ]]; then
			./$outputfilename
			echo "Vuoi ricompilare e rieseguire lo stesso file sovrascrivendo quello appena compilato? (ricordati di salvare le modifice al file C per aprezzare la differenza) [y/n]"
			read retry
			while [[ $retry == y ]]; do
				gcc -O2 -pedantic -std=c89 -Wall -Wno-unused-result -Wno-deprecated-declarations $inputfilename -o $outputfilename -lm
				./$outputfilename
				echo "Vuoi ricompilare e rieseguire lo stesso file sovrascrivendo quello appena compilato? (ricordati di salvare le modifice al file C per aprezzare la differenza) [y/n]"
				read retry
			done
		fi
		restart2=n
	elif [[ $howmanyfiles == 0 ]]; then
		echo "Non ci sono file C in questa cartella"
		echo "Vuoi riprovare? Stavolta metti il file che vuoi compilare in questa cartella [y/n]"
		read restart2
		if [[ $restart2 == y ]]; then
			echo "Riavvio la ricerca di file C..."
		else
			echo "Ok, arrivederci allora"
			exit
		fi
	else
		echo "Come vuoi chiamare il file compilato?"
		read outputfilename
		gcc -O2 -pedantic -std=c89 -Wall -Wno-unused-result -Wno-deprecated-declarations *.c -o $outputfilename -lm
		echo "Vuoi eseguire il file appena compilato? [y/n]"
		read answer
		if [[ $answer == y ]]; then
			./$outputfilename
			echo "Vuoi ricompilare e rieseguire lo stesso file sovrascrivendo quello appena compilato? (ricordati di salvare le modifice al file C per aprezzare la differenza) [y/n]"
			read retry
			while [[ $retry == y ]]; do
				gcc -O2 -pedantic -std=c89 -Wall -Wno-unused-result -Wno-deprecated-declarations *.c -o $outputfilename -lm
				./$outputfilename
				echo "Vuoi ricompilare e rieseguire lo stesso file sovrascrivendo quello appena compilato? (ricordati di salvare le modifice al file C per aprezzare la differenza) [y/n]"
				read retry
			done
		fi
		restart2=n
	fi
	if [[ $restart2 != y ]]; then
		echo "Vuoi compilare ed eventualmente eseguire un altro file? [y/n]"
		read restart
	fi
done
echo "Ok, arrivederci allora"
exit
