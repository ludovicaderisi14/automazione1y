#include <stdio.h>
#define NOMEFILE2019 "./scatoloni2019.txt"
#define MAXPEZZI 100
#define NTIPI 5
#define LUMINOSE 'L'
#define ROSSE 'R'
#define BLU 'B'
#define VERDI 'V'
#define GHIRLANDE 'G'
#define QTA_L 10
#define QTA_R 5
#define QTA_B 3
#define QTA_V 4
#define QTA_G 3

typedef struct pezzi_s {
	int pezziPerTipo, contaPezzi;
	char nome;
} pezzi_t;

int riorganizzaScatoloni (char[], int*);

int main (int argc, char * argv[])
{
	FILE *f2019;
	char scatolone[MAXPEZZI + 1];
	int numeroSet, numeroPezzi, tmpPezzi, tmpSet;

	if ((f2019 = fopen (NOMEFILE2019, "r"))) {
		numeroSet = 0;
		numeroPezzi = 0;
		while (fscanf (f2019, "%s", scatolone) != EOF) {
			tmpSet = riorganizzaScatoloni (scatolone, &tmpPezzi);
			numeroSet += tmpSet;
			numeroPezzi += tmpPezzi;
		}
		printf ("Puoi fare %d set e ti rimarranno %d pezzi\n", numeroSet, numeroPezzi);
		fclose(f2019);
	} else {
		printf ("Impossibile accedere al file %s\n", NOMEFILE2019);
	}
	
	return 0;
}

int riorganizzaScatoloni (char scatolone[], int *pezziRimanenti)
{
	pezzi_t pezzi [NTIPI];
	int i, j, numeroSet, tmp;

	pezzi[0/* Queste pure potevano essere define*/].pezziPerTipo = QTA_L;
	pezzi[1].pezziPerTipo = QTA_R;
	pezzi[2].pezziPerTipo = QTA_B;
	pezzi[3].pezziPerTipo = QTA_V;
	pezzi[4].pezziPerTipo = QTA_G;

	pezzi[0].nome = LUMINOSE;
	pezzi[1].nome = ROSSE;
	pezzi[2].nome = BLU;
	pezzi[3].nome = VERDI;
	pezzi[4].nome = GHIRLANDE;

	for (i = 0; i < NTIPI; i++) {
		pezzi[i].contaPezzi = 0;
	}

	for (i = 0; scatolone[i] != '\0'; i++) {
		for (j = 0; j < NTIPI; j++) {
			if (scatolone[i] == pezzi[j].nome) {
				pezzi[j].contaPezzi++;
			}
		}
	}

	numeroSet = pezzi[0].contaPezzi / pezzi[0].pezziPerTipo;
	*pezziRimanenti = 0;

	for (i = 1; i < NTIPI; i++) {
		tmp = pezzi[i].contaPezzi / pezzi[i].pezziPerTipo;
		if (tmp < numeroSet) {
			numeroSet = tmp;
		}
	}

	if (numeroSet != 0) {
		for (i = 0; i < NTIPI; i++) {
			*pezziRimanenti += pezzi[i].contaPezzi % (pezzi[i].pezziPerTipo * numeroSet);
		}
	} else {
		for (i = 0; i < NTIPI; i++) {
			*pezziRimanenti += pezzi[i].contaPezzi;
		}
	}

	return numeroSet;
}
