#include <stdio.h>
#define DIM 4

int main(int argc, char * argv[])
{
	int mat[DIM][DIM], ris[DIM][DIM];
	int i, j, identita, tmp;
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}
	/*for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			ris[j][i] = mat[i][j];
			ris[i][j] = mat[j][i];
		}
	}*/
	for (i = 0; i < DIM; i++) {
		for (j = i + 1; j < DIM; j++) {
			tmp = mat[i][j];
			mat[i][j] = mat[j][i];
			mat[j][i] = tmp; /* Così modifica la matrice di partenza ed era più difficile */
		}
	}
	printf ("\n");
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			printf("%d ", mat[i][j]);
		}
		printf ("\n");
	}
	printf ("\n");
	return 0;
}
