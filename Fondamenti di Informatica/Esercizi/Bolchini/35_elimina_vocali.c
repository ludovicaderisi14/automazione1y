/* Acquisita una stringa di al più 25 caratteri, elimina tutte le vocali, quindi la visualizza */
#include <stdio.h>
#define LMAX 25

int main(int argc, char * argv[])
{
	char seq[LMAX+1];
	int dim, i;
	scanf("%s", seq);
	i = 0;
	for (dim = 0; seq[dim] != '\0'; dim++) {
		if (seq[dim] != 'a' && seq[dim] != 'e' && seq[dim] && 'i' && seq[dim] != 'o' && seq[dim] != 'u') {
			seq[i] = seq[dim];
			i++;
		}
	}
	seq[i] = '\0';
	printf("%s\n", seq);
	return 0;
}
