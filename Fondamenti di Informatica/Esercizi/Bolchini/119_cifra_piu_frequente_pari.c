#include <stdio.h>
#define CIFRE 5
#define BASE 10

int cifra (int);

int main (int argc, char * argv[])
{
    int val, ris;

    scanf ("%d", &val);
    ris = cifra (val);
    printf ("%d\n", ris);

    return 0;
}

int cifra (int val)
{
    int cifre_pari[CIFRE], cifre_dispari[CIFRE], cifra, i, ris, exist_pari;

    for (i = 0; i < CIFRE; i++) {
        cifre_pari[i] = 0;
    }
    for (i = 0; i < CIFRE; i++) {
        cifre_dispari[i] = 0;
    }

    exist_pari = 0;
    while (val > 0) {
        cifra = val % BASE;
        if (cifra % 2 == 0) {
            cifre_pari[cifra / 2]++;
            exist_pari++;
        } else {
            cifre_dispari[cifra / 2]++; 
        }
        val = val / BASE;
    }

    ris = 0;
    if (exist_pari) {
        for (i = 1; i < CIFRE; i++) {
            if (cifre_pari[i] > cifre_pari[ris]) {
                ris = i;
            }
        }
        ris = ris * 2;
    } else {
        ris = 0;
        for (i = 1; i < CIFRE; i++) {
            if (cifre_dispari[i] > cifre_dispari[ris]) {
                ris = i;
            }
        }
    }
    if (!ris) {
        ris++;
    } else {
        ris = ris * 2 + 1;
    }

    return ris;
}