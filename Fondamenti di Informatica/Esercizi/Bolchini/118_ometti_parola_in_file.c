#include <stdio.h>
#include <ctype.h>
#define OPTIONS 4
#define MAXCHAR 30
#define UPPERINIT 'A'
#define LOWERINIT 'a'

int ometti (char [], char [], char []);
int sonouguali (char [], char []);

int main (int argc, char * argv [])
{
    int ris;

    if (argc == OPTIONS) {
        ris = ometti (argv[1], argv[2], argv[3]);
        printf ("%d\n", ris);
    } else {
        printf ("Numero di argomenti sbagliato\n");
    }
    

    return 0;
}

int ometti (char src[], char dst[], char voc[])
{
    FILE *fin, *fout;
    char curWord[MAXCHAR+1];
    int ris;

    if ((fin = fopen (src, "r"))) {
        if ((fout = fopen (dst, "w"))) {
            ris = 0;
            while (fscanf (fin, "%s", curWord) != EOF) {
                if (!sonouguali(voc, curWord)) {
                    fprintf (fout, "%s ", curWord);
                } else {
                    ris++;
                }
            }
        } else {
            printf ("ERRORE\n");
            ris = -1;
        }
    } else {
        printf ("ERRORE\n");
        ris = -1;
    }

    return ris;
}

int sonouguali (char seq1[], char seq2[])
{
    int i, ris, char1_val, char2_val;

    ris = 1;
    for (i = 0; seq1[i] != '\0' && ris; i++) {
        if (isupper (seq1[i])) {
            char1_val = seq1[i] - UPPERINIT;
        } else {
            char1_val = seq1[i] - LOWERINIT;
        }
        if (isupper (seq2[i])) {
            char2_val = seq2[i] - UPPERINIT;
        } else {
            char2_val = seq2[i] - LOWERINIT;
        }
        if (char1_val != char2_val) {
            ris = 0;
        }
    }

    if (seq2[i] != '\0') {
        ris = 0;
    }

    return ris;
}