/* Prende un array di 10 interi e ne associa uno che ad ogni valore dice se è perfetto (1) abbondante (2) difettivo (3) o negativo (0), poi ne associa un altro con quanti sono di ogni tipo */
#include <stdio.h>
#define NMAX 10
#define OPZIONI 3

int tipo (int);

int main(int argc, char * argv[])
{
	int seq[NMAX], i, ris[NMAX], ris_occorrenze[OPZIONI];

	for (i = 0; i < NMAX; i++) {
		scanf("%d", &seq[i]);
		ris[i] = tipo(seq[i]);
		if (ris[i] == 1) {
			ris_occorrenze[0]++;
		} else if (ris[i] == 2) {
			ris_occorrenze[1]++;
		} else if (ris[i] == 3) {
			ris_occorrenze[2]++;
		}
	}

	for (i = 0; i < NMAX; i++) {
		printf("%d ", ris[i]);
	}
	printf("\n");
	for (i = 0; i < OPZIONI; i++) {
		printf("%d ", ris_occorrenze[i]);
	}
	printf("\n");

	return 0;
}

int tipo (int num)
{
	int ris, i, div;

	if (num > 0) {
		div = 1;
		for (i = 2; i < num; i++) {
			if (num % i == 0) {
				div = div + i;
			}
		}
		if (div == num) {
			ris = 1;
		} else if (div > num) {
			ris = 2;
		} else {
			ris = 3;
		}
	} else {
		ris = 0;
	}

	return ris;
}