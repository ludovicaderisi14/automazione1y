#include <stdio.h>
#define DIPENDENTI 4
#define ORE 24

typedef struct dip_s {
	int entrata, uscita;
} dip_t;

int main (int argc, char * argv[])
{
	dip_t dipendente;
	int count [ORE], i, j, ris;

	for (i = 0; i < ORE; i++) {
		count[i] = 0;
	}

	for (i = 0; i < DIPENDENTI; i++) {
		scanf ("%d%d", &dipendente.entrata, &dipendente.uscita);
		for (j = dipendente.entrata; j < dipendente.uscita; j++) {
			count[j]++;
		}
		printf ("\n");
	}

	ris = 0;
	for (i = 0; i < ORE; i++) {
		if (count[i] > count[ris]) {
			ris = i;
		}
	}

	printf ("%d\n", ris);

	return 0;
}
