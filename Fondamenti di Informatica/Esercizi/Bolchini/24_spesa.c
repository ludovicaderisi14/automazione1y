#include <stdio.h>
#define NMAX 50
#define STOP 0

int main(int argc, char * argv[])
{
    float val[NMAX], prezzo, costo_totale, max;
    int i, dim, num_pezzi, omaggi_esclusi;
    
    dim = 0;
    scanf("%f", &prezzo);
    while(prezzo >= STOP && dim < NMAX){
        val[dim] = prezzo;
        dim++;
        scanf("%f", &prezzo);
    }
    if (dim > 0) {
		do {
			printf("Ora il valore massimo maggiore o uguale a zero\n");
			scanf("%f", &max);
		} while (max < 0);
	}
	costo_totale = 0;
	omaggi_esclusi = 0;
	if (max > 0) {
		for(num_pezzi = 0; num_pezzi < dim && costo_totale < max; num_pezzi++) {
			costo_totale = costo_totale + val[num_pezzi];
		}
		for (i = num_pezzi; i < dim; i++) {
			if (val[i] == 0) {
				omaggi_esclusi++;
			}
		}
		if (costo_totale > max) {
			num_pezzi--;
			costo_totale = costo_totale - val[num_pezzi];
		}
		num_pezzi = num_pezzi + omaggi_esclusi;
	} else {
		for(num_pezzi = 0; num_pezzi < dim && costo_totale <= max; num_pezzi++) {
			costo_totale = costo_totale + val[num_pezzi];
		}
		costo_totale = 0;
		for (i = num_pezzi; i < dim; i++) {
			if (val[i] == 0) {
				omaggi_esclusi++;
			}
		}
		if (dim > 0) {
			num_pezzi = num_pezzi + omaggi_esclusi - 1;
		}
	}
    printf("%d: %f\n", num_pezzi, costo_totale);
    return 0;
}
