#include <stdio.h>
#include <stdlib.h>
#include "list_int.h"
#define STOP 0

int primaDiVal (lista_t *, int);

int main (int argc, char * argv[])
{
	lista_t *head = NULL;
	int val, ris;

	scanf ("%d", &val);
	while (val != STOP) {
		head = append (head, val);
		scanf ("%d", &val);
	}
	printf ("\n");
	scanf ("%d", &val);

	ris = primaDiVal (head, val);

	printf ("%d\n", ris);

	head = empty (head);
	
	return 0;
}

int primaDiVal (lista_t *head, int val)
{
	int ris, tmp;
	
	if (!head) {
		ris = -1;
	} else if (head->val == val) {
		ris = 0;
	} else {
		tmp = primaDiVal (head->next, val);
		if (tmp == -1) {
			ris = tmp;
		} else {
			ris = tmp + 1;
		}
	}

	return ris;
}