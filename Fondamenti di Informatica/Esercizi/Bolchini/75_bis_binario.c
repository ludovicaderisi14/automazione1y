/* Anche se sprechi memoria a salvare tutto è più efficiente che accedere al file un sacco d volte */
#include <stdio.h>
#include <string.h>
#define CMAX 50
#define INGREDIENTI 10
#define RICETTE 200
#define INPUT "./ricette.dat"
#define INPUT_DUE "./frigo.dat"
#define OUTPUT "posso_cucinare.dat"

typedef struct ingrediente_s {
	char nome[CMAX+1];
	float qta;
} ingrediente_t;

typedef struct ricetta_s { /* Nei file binari sta cosa occupa un numero fisso di byte quindi è facile saltare da un punto a un altro, inoltre si possono salvare più cose alla volta */
	char nome[CMAX+1];
	int nPersone, nIngredienti;
	ingrediente_t ingredienti[INGREDIENTI];
} ricetta_t;

int possoCucinare (ricetta_t, ingrediente_t [], int);

int main(int argc, char * argv[])
{
	ricetta_t ric [RICETTE], ricCucinabili [RICETTE];
	ingrediente_t dafrigo [INGREDIENTI];
	FILE *fric, *ffrig, *fout;
	int i, count, numIngredienti, numRicetteFattibili;

	if (fric = fopen (INPUT, "r")) {
		if (ffrig = fopen (INPUT_DUE, "r")) {
			if (fout = fopen (OUTPUT, "w")) {
				/* Aquisisce dal file degli ingredienti */
				fread (dafrigo, sizeof (ingrediente_t), INGREDIENTI, ffrig); /* Legge dal file ffrig INGREDIENTI volte un insieme di byte lungo sizeof (...) e mette gli insiemi nell'array dafrigo */
				numRicetteFattibili = 0;
				/* Aquisisce la ricetta */
				fread (ric, sizeof (ricetta_t), RICETTE, fric);
				/* Se posso cucinare scrivo sul file la ricetta */
				for (i = 0; i < RICETTE; i++) {
					if (possoCucinare(ric, dafrigo, numIngredienti)) {
						ricCucinabili [numRicetteFattibili] = ric [i]; /* Copia l'intero contenuto dela struttura nell'altro array */
						numRicetteFattibili++;
					}
				}
				fwrite (ricCucinabili, sizeof (ricetta_t), numRicetteFattibili, fout);
				printf ("Le ricette totali sono %d, mentre quelle che puoi cucinare sono %d\n", count, numRicetteFattibili);
			} else {
				printf ("Problemi con il file %s\n", OUTPUT);
			}
			fclose (ffrig);
		} else {
			printf ("Problemi con il file %s\n", INPUT_DUE);
		}
		fclose (fric);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}

int possoCucinare (ricetta_t ric, ingrediente_t ing[], int dim)
{
	int ris, trovato, i, j;

	if (dim >= ric.nIngredienti) { /* è solo un'ottimizzazione */
		ris = 1;
		for (i = 0; i < ric.nIngredienti && ris == 1; i++) {
			trovato = 0;
			for (j = 0; i < dim && trovato == 0; j++) {
				if (strcmp (ric.ingredienti[i].nome, ing[j].nome) && ric.ingredienti[i].qta <= ing[j].qta) {
					trovato = 1;
				}
			}
			ris = trovato;
		}
	} else {
		ris = 0;
	}
	
	return ris;
}

