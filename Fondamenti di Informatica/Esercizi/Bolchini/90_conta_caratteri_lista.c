#include <stdio.h>
#include <stdlib.h>
#include "list_char.h"
#define STOP '.'

void contaCaratteri (lista_t *);

int main (int argc, char * argv[])
{
	lista_t * head = NULL;
	char val;

	scanf ("%c%*c", &val);
	while (val != STOP) {
		head = append (head, val);
		scanf ("%c%*c", &val);
	}
	printl (head);
	contaCaratteri (head);

	head = empty (head);
	
	return 0;
}

void contaCaratteri (lista_t *head)
{
	lista_t *ptr;
	int trovato;

	ptr = head;
	while (ptr) {
		printf ("%c: ", ptr->val);
		trovato = 1;
		while (ptr->next && (ptr->val == ptr->next->val)) {
			ptr = ptr->next;
			trovato++;
		}
		printf ("%d\n", trovato);
		ptr = ptr->next;
	}

	return;
}