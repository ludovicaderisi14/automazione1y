/* da 251 a 152 ignorando il rischio di overflow. Voglio calcolarlo e metterlo in una variabile. Da 9000 a 9. */
#include <stdio.h>
#define BASE 10

int main(int argc, char * argv[])
{
	int val, cifra, bkp, valrev;
	scanf("%d", &val);
	bkp = val;
	valrev = 0;
	while (val > 0) {
		cifra = val % BASE;
		valrev = valrev * BASE + cifra;
		val = val / BASE;
	}
	printf("\n");
	return 0;
}