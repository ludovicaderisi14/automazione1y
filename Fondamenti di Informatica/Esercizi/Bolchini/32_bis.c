#include <stdio.h>
#define DIM 10

int main(int argc, char * argv[])
{
	int mat[DIM], i, j, trg, count;
	for (i = 0; i < DIM; i++) {
		scanf ("%d", &mat[i]);
	}
	scanf("%d", &trg);
	count = 0;
	for (i = 0; i < (DIM - 1); i++) {
		for (j = i + 1; j < DIM; j++) {
			if (mat[i] + mat[j] == trg) {
				count++;
			}
		}
	}
	count = count * 2; /* così non gli faccio contare l'ordine */
	printf("%d\n", count);
	return 0;
}